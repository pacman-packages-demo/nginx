# nginx

Web and reverse proxy server [nginx](https://www.archlinux.org/packages/?q=nginx)

* [nginx](https://wiki.archlinux.org/index.php/nginx) wiki.archlinux.org

# Missing /etc/init.d/nginx file
* [/etc/init.d/nginx
  ](https://google.com/search?q=%2Fetc%2Finit.d%2Fnginx)

## nginx
* [*NGINX Init Scripts*
  ](https://www.nginx.com/resources/wiki/start/topics/examples/initscripts/)

## Artix
* Take the files from Gentoo!
* [nginx site:artixlinux.org
  ](https://www.google.com/search?q=nginx+site%3Aartixlinux.org)
  * [*Topic: How to autorun?*
    ](https://forum.artixlinux.org/index.php/topic,294.0.html)
    2017 Artix Linux Forum
